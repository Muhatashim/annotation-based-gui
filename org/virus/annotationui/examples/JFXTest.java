package org.virus.annotationui.examples;/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/19/13
 * Time: 2:31 PM
 */

import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import org.virus.annotationui.annotations.ActionButton;
import org.virus.annotationui.annotations.ID;
import org.virus.annotationui.annotations.LabelFor;
import org.virus.annotationui.builders.jfx.JFXBuilder;

import java.util.HashMap;

public class JFXTest extends Application {

    @LabelFor("Runescape Username")
    @ID("user")
    private String user;

    @LabelFor("Runescape Password")
    @ID("pass")
    private String pass;

    @LabelFor("GP desired: ")
    @ID("geepee")
    private int geepeez;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        FlowPane content = new FlowPane(Orientation.VERTICAL);
        HashMap<Class<?>, Object> classInstances = new HashMap<>();

        classInstances.put(this.getClass(), this);

        JFXBuilder jfxBuilder = new JFXBuilder(classInstances, content);
        jfxBuilder.initUI();

        Scene scene = new Scene(content);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @ActionButton("Hack some gee pee")
    @ID("up button")
    private void sendUserAndPass() {
        /*some awesome secrete code*/
        System.out.printf("Sent %s, %s to query. You will receive your %d geepeez within 24hrs.\n", user, pass, geepeez);
        System.exit(0);
    }
}
