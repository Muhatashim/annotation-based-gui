package org.virus.annotationui.builders;

import org.virus.annotationui.utils.Box;

import java.lang.annotation.Annotation;
import java.lang.reflect.Member;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/20/13
 * Time: 10:51 AM
 */
public interface AnnotationHandler<T extends Annotation, S, U extends UIBuilder<S>> {

    void handle(T annotation,
             U builder,
             Box<? super S> family,
             S widgetCreated,
             Member member,
             Object classInstance);
}
