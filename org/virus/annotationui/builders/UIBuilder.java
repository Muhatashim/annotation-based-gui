package org.virus.annotationui.builders;

import org.virus.annotationui.annotations.*;
import org.virus.annotationui.utils.Box;
import org.virus.annotationui.utils.Filter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/18/13
 * Time: 6:35 PM
 */
public abstract class UIBuilder<T> {

    private final HashMap<Class<?>, Object>       classesWithUIAnnotations;
    private final HashMap<String, Box<? super T>> uiWidgets;
    private final LinkedList<BuildInfo>           buildInfos;

    /**
     * @param classesWithUIAnnotations Class with UI annotation, instance of that class or can be null if all ui members are static
     */
    public UIBuilder(HashMap<Class<?>, Object> classesWithUIAnnotations) {
        this.classesWithUIAnnotations = classesWithUIAnnotations;
        uiWidgets = new HashMap<>();
        buildInfos = new LinkedList<>();
    }

    public void initUI() {
        setupUI();
        buildWidgets();
    }

    protected abstract T buildWidget(Member member, Annotation[] annotations, Object classInstance);

    private void buildWidgets() {
        HashMap<BuildInfo, Integer> tries = new HashMap<>();

        while (buildInfos.peek() != null) {
            BuildInfo info = buildInfos.pop();
            Integer try_ = tries.get(info);

            if (try_ != null && try_.equals(10))
                throw new UnknownError("Could not create widget. " + info);

            T t = buildWidget(info.member, info.uiAnnotations, info.classInstance);

            if (t == null) {
                buildInfos.add(info);
                tries.put(info, try_ != null ? try_ + 1 : 1);
                continue;
            }

            uiWidgets.get(info.id).setBoxedObject(t);
        }
    }

    private void setupUI() {
        for (Map.Entry<Class<?>, Object> entry : classesWithUIAnnotations.entrySet()) {
            Class<?> key = entry.getKey();
            final Object value = entry.getValue();

            Field[] fields = key.getDeclaredFields();
            Method[] methods = key.getDeclaredMethods();

            Filter<Member> memberFilter = new Filter<Member>() {
                Annotation[] uiAnnotations;

                @Override
                public boolean accept(Member element) {
                    uiAnnotations = getUIAnnotations(element);
                    return uiAnnotations.length > 0;
                }

                @Override
                public boolean run(Member t) {
                    String id = null;

                    for (Annotation annotation : uiAnnotations)
                        if (annotation instanceof ID)
                            id = (((ID) annotation).value());

                    if (id == null)
                        throw new NullPointerException("Id cannot be null! " + t);

                    uiWidgets.put(id, new Box<T>());
                    buildInfos.add(new BuildInfo(id, t, uiAnnotations, value));
                    return false;
                }
            };

            memberFilter.loop(fields);
            memberFilter.loop(methods);
        }
    }

    private Annotation[] getUIAnnotations(Member member) {
        List<Annotation> annotationsList = new ArrayList<>();

        Annotation[] annotations;
        if (member instanceof Field)
            annotations = ((Field) member).getDeclaredAnnotations();
        else if (member instanceof Method)
            annotations = ((Method) member).getDeclaredAnnotations();
        else
            throw new IllegalArgumentException();

        for (Annotation annotation : annotations)
            if (annotation instanceof ActionButton
                    || annotation instanceof Group
                    || annotation instanceof LabelFor
                    || annotation instanceof ID
                    || annotation instanceof Tab)
                annotationsList.add(annotation);

        return annotationsList.toArray(new Annotation[annotationsList.size()]);
    }

    public final Box<? super T> putWidget(String id, Box<? super T> widget) {
        return uiWidgets.put(id, widget);
    }

    public final Box<? super T> putWidget(ID id, Box<? super T> widget) {
        return putWidget(id.value(), widget);
    }

    public final Box<? super T> findWidgetBy(String id) {
        return uiWidgets.get(id);
    }

    public final Box<? super T> findWidgetBy(ID id) {
        return findWidgetBy(id.value());
    }

    private static class BuildInfo {
        private final String       id;
        private final Member       member;
        private final Annotation[] uiAnnotations;
        private final Object       classInstance;

        public BuildInfo(String id, Member member, Annotation[] uiAnnotations, Object classInstance) {
            this.id = id;
            this.member = member;
            this.uiAnnotations = uiAnnotations;
            this.classInstance = classInstance;
        }

        @Override
        public String toString() {
            return "BuildInfo{" +
                    "id='" + id + '\'' +
                    ", member=" + member +
                    ", uiAnnotations=" + Arrays.toString(uiAnnotations) +
                    ", classInstance=" + classInstance +
                    '}';
        }
    }
}
