package org.virus.annotationui.builders;

import org.virus.annotationui.annotations.*;
import org.virus.annotationui.utils.Box;

import java.lang.annotation.Annotation;
import java.lang.reflect.Member;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/20/13
 * Time: 11:50 AM
 */
public class AnnotationController<S extends UIBuilder<U>, U> {
    private final HashMap<Class<? extends Annotation>,
            ? super AnnotationHandler<? extends Annotation, U, S>> annotationHandlerTypes;

    public AnnotationController(AnnotationHandler<ActionButton, U, S> actionHandler,
                                AnnotationHandler<Family, U, S> familyHandler,
                                AnnotationHandler<Group, U, S> groupHandler,
                                AnnotationHandler<LabelFor, U, S> labelForHandler,
                                AnnotationHandler<Tab, U, S> tabHandler) {

        annotationHandlerTypes = new HashMap<>(5);

        annotationHandlerTypes.put(ActionButton.class, actionHandler);
        annotationHandlerTypes.put(Family.class, familyHandler);
        annotationHandlerTypes.put(Group.class, groupHandler);
        annotationHandlerTypes.put(LabelFor.class, labelForHandler);
        annotationHandlerTypes.put(Tab.class, tabHandler);
    }

    public void handle(Annotation annotation,
                       S builder,
                       Box<? super U> family,
                       U widgetCreated,
                       Member member,
                       Object classInstance) throws Exception {

        AnnotationHandler<Annotation, U, S> handler =
                (AnnotationHandler<Annotation, U, S>) annotationHandlerTypes.get(annotation.annotationType());
        if (handler != null) {
            handler.handle(annotation, builder, family, widgetCreated, member, classInstance);
        } else
            Logger.getAnonymousLogger().fine(String.format("No %s handler for %s.%s.",
                    annotation.annotationType(),
                    member.getDeclaringClass().getName(),
                    member.getName()));
    }
}
