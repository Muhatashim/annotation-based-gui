package org.virus.annotationui.builders.jfx;

import javafx.scene.Node;
import org.virus.annotationui.builders.AnnotationHandler;

import java.lang.annotation.Annotation;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/20/13
 * Time: 11:11 AM
 */
public interface JFXAnnotationHandler<T extends Annotation> extends AnnotationHandler<T, Node, JFXBuilder> {
}
