package org.virus.annotationui.builders.jfx.annotation_handlers;

import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.TabPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import org.virus.annotationui.annotations.Tab;
import org.virus.annotationui.builders.jfx.JFXAnnotationHandler;
import org.virus.annotationui.builders.jfx.JFXBuilder;
import org.virus.annotationui.utils.Box;

import java.lang.reflect.Member;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/20/13
 * Time: 10:50 AM
 */
public class TabHandler implements JFXAnnotationHandler<Tab> {
    @Override
    public void handle(Tab annotation,
                       JFXBuilder builder,
                       Box<? super Node> family,
                       Node widgetCreated,
                       Member member,
                       Object classInstance) {

        Box<? super Node> boxedTabPane = builder.findWidgetBy(annotation.value());

        if (boxedTabPane != null) {
            TabPane tabPane = (TabPane) boxedTabPane.getBoxedObject();

            for (javafx.scene.control.Tab tab : tabPane.getTabs())
                if (tab.getText().equals(annotation.tabName())) {
                    family.setBoxedObject(tab.getContent());

                    return;
                }
            family.setBoxedObject(createTab(tabPane, annotation));
            return;
        }

        TabPane tabPane = createTabPane();

        family.<Pane>castAndGetBoxedObject().getChildren().add(tabPane);
        family.setBoxedObject(createTab(tabPane, annotation));
        builder.putWidget(annotation.value(), new Box<Node>(tabPane));
    }


    private Node createTab(TabPane pane, Tab tabAnnotation) {
        javafx.scene.control.Tab tab = new javafx.scene.control.Tab(tabAnnotation.tabName()) {{
            setContent(new FlowPane(Orientation.VERTICAL));
        }};
        pane.getTabs().add(tab);

        return tab.getContent();
    }

    private TabPane createTabPane() {
        return new TabPane() {{
            setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        }};
    }
}
