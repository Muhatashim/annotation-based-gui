package org.virus.annotationui.builders.jfx.annotation_handlers;

import javafx.scene.Node;
import org.virus.annotationui.annotations.Family;
import org.virus.annotationui.builders.jfx.JFXAnnotationHandler;
import org.virus.annotationui.builders.jfx.JFXBuilder;
import org.virus.annotationui.utils.Box;

import java.lang.reflect.Member;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/20/13
 * Time: 10:50 AM
 */
public class FamilyHandler implements JFXAnnotationHandler<Family> {

    @Override
    public void handle(Family annotation,
                       JFXBuilder builder,
                       Box<? super Node> family,
                       Node widgetCreated,
                       Member member,
                       Object classInstance) {

        Box<? super Node> familyBoxNode = builder.findWidgetBy((annotation.value()));

        if (familyBoxNode == null)
            throw new NullPointerException();

        family.setBoxedObject(familyBoxNode.<Node>castAndGetBoxedObject());
    }
}
