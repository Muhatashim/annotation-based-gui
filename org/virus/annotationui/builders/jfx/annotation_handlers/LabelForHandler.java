package org.virus.annotationui.builders.jfx.annotation_handlers;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import org.virus.annotationui.annotations.LabelFor;
import org.virus.annotationui.builders.jfx.JFXAnnotationHandler;
import org.virus.annotationui.builders.jfx.JFXBuilder;
import org.virus.annotationui.utils.Box;

import java.lang.reflect.Member;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/20/13
 * Time: 10:56 AM
 */
public class LabelForHandler implements JFXAnnotationHandler<LabelFor> {

    @Override
    public void handle(final LabelFor annotation,
                       JFXBuilder builder,
                       Box<? super Node> family,
                       Node widgetCreated,
                       Member member,
                       Object classInstance) {

        HBox labelBox = new HBox(10) {{
            getChildren().add(new Label(annotation.value()));
        }};
        Pane pane = family.castAndGetBoxedObject();
        pane.getChildren().add(labelBox);
        family.setBoxedObject(labelBox);
    }
}
