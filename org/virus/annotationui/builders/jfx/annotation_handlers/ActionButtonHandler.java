package org.virus.annotationui.builders.jfx.annotation_handlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import org.virus.annotationui.annotations.ActionButton;
import org.virus.annotationui.builders.jfx.JFXAnnotationHandler;
import org.virus.annotationui.builders.jfx.JFXBuilder;
import org.virus.annotationui.utils.Box;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/20/13
 * Time: 10:50 AM
 */
public class ActionButtonHandler implements JFXAnnotationHandler<ActionButton> {

    @Override
    public void handle(ActionButton annotation,
                       JFXBuilder builder,
                       Box<? super Node> family,
                       Node widgetCreated,
                       Member member,
                       final Object classInstance) {

        if (!(widgetCreated instanceof Button) || !(member instanceof Method))
            throw new ClassCastException();

        final Method method = (Method) member;
        Button button = (Button) widgetCreated;

        button.setText(annotation.value());
        method.setAccessible(true);

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    method.invoke(classInstance);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
