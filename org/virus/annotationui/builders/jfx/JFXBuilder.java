package org.virus.annotationui.builders.jfx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import org.virus.annotationui.annotations.Tab;
import org.virus.annotationui.builders.AnnotationController;
import org.virus.annotationui.builders.UIBuilder;
import org.virus.annotationui.builders.jfx.annotation_handlers.ActionButtonHandler;
import org.virus.annotationui.builders.jfx.annotation_handlers.FamilyHandler;
import org.virus.annotationui.builders.jfx.annotation_handlers.LabelForHandler;
import org.virus.annotationui.builders.jfx.annotation_handlers.TabHandler;
import org.virus.annotationui.utils.Box;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/18/13
 * Time: 6:41 PM
 */
public class JFXBuilder extends UIBuilder<Node> {

    private final Pane superParent;

    private final AnnotationController<JFXBuilder, Node> annotationController;

    /**
     * @param classesWithUIAnnotations Class with UI annotation, instance of that class or can be null if all ui members are static
     */
    public JFXBuilder(HashMap<Class<?>, Object> classesWithUIAnnotations, Pane superParent) {
        super(classesWithUIAnnotations);
        this.superParent = superParent;

        annotationController = new AnnotationController<>(
                new ActionButtonHandler(),
                new FamilyHandler(),
                null,
                new LabelForHandler(),
                new TabHandler()
        );
    }

    @Override
    protected Node buildWidget(final Member member, Annotation[] annotations, final Object classInstance) {
        Box<Node> familyContainer = new Box<Node>(superParent);
        Node node;

        if (member instanceof Field)
            node = makeMemberToNode((Field) member, classInstance);
        else
            node = new Button();

        for (final Annotation annotation : annotations)
            try {
                annotationController.handle(annotation, this, familyContainer, node, member, classInstance);
            } catch (Exception e) {
                e.printStackTrace();
            }

        familyContainer.<Pane>castAndGetBoxedObject().getChildren().add(node);
        return node;
    }

    private Node makeMemberToNode(final Field field, final Object classInstance) {
        final TextField valueArea = new TextField();
        final Class<?> type = field.getType();

        field.setAccessible(true);

        if (type == String.class) {
            valueArea.setPromptText("Enter some text...");

            valueArea.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String s2) {
                    try {
                        field.set(classInstance, s2);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else if (type.isPrimitive() && type != boolean.class) {
            valueArea.setPromptText("Enter a number");

            valueArea.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String s2) {
                    try {
                        _typeCheck:
                        {
                            if (type == int.class) {
                                field.set(classInstance, Integer.parseInt(s2));
                                break _typeCheck;
                            }
                            if (type == float.class) {
                                field.set(classInstance, Float.parseFloat(s2));
                                break _typeCheck;
                            }
                            if (type == double.class) {
                                field.set(classInstance, Double.parseDouble(s2));
                                break _typeCheck;
                            }
                            if (type == long.class) {
                                field.set(classInstance, Long.parseLong(s2));
                                break _typeCheck;
                            }
                            if (type == short.class) {
                                field.set(classInstance, Short.parseShort(s2));
                                break _typeCheck;
                            }
                            if (type == byte.class) {
                                field.set(classInstance, Byte.parseByte(s2));
                                break _typeCheck;
                            }
                        }
                    } catch (IllegalAccessException | NumberFormatException e) {
                        if (!(e instanceof NumberFormatException))
                            e.printStackTrace();
                        valueArea.setText(s);
                    }
                }
            });
        }
        return valueArea;
    }

    private Node createTab(TabPane pane, Tab tabAnnotation) {
        javafx.scene.control.Tab tab = new javafx.scene.control.Tab(tabAnnotation.tabName()) {{
            setContent(new FlowPane(Orientation.VERTICAL));
        }};
        pane.getTabs().add(tab);

        return tab.getContent();
    }

    private TabPane createTabPane() {
        return new TabPane() {{
            setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        }};
    }
}
