package org.virus.annotationui.annotations;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/18/13
 * Time: 6:26 PM
 */
public enum GroupType {
    HORIZONTAL_GROUP,
    VERTICAL_GROUP
}
