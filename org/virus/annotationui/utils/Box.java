package org.virus.annotationui.utils;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 7/19/13
 * Time: 11:50 AM
 */
public class Box<T> {
    private T boxedObject;

    public Box() {
    }

    public Box(T boxedObject) {
        this.boxedObject = boxedObject;
    }

    public <S extends T> S castAndGetBoxedObject() {
        return (S) boxedObject;
    }

    public T getBoxedObject() {
        return boxedObject;
    }

    public T setBoxedObject(T boxedObject) {
        return this.boxedObject = boxedObject;
    }
}
